// +----------------------------------------------------------------------
// | Dkpic [ WE CAN DO IT JUST KAN ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://www.dingkan.net All rights reserved.
// +----------------------------------------------------------------------
// | Author: DingKan <dingkan@tom.com>
// +----------------------------------------------------------------------
	(function(w, d){
		function Dkpic (name, size, color) {

			name  = name || '';
			//图片大小
			size  = size ||  $(".Dkpic").width() ||  $(".Dkpic").height();
			//背景颜色
			var colours = [
					"#1abc9c","#2ecc71","#3498db","#9b59b6","#34495e","#16a085","#27ae60","#2980b9","#8e44ad","#2c3e50",
					"#f1c40f","#e67e22","#e74c3c","#ecf0f1","#95a5a6","#f39c12","#d35400","#c0392b","#bdc3c7","#7f8c8d",
					"#DAA520","#808080","#008000","#ADFF2F","#F0FFF0","#FF69B4","#CD5C5C","#4B0082","#FFFFF0","#F0E68C","#E6E6FA",
					"#FFF0F5","#7CFC00","#FFFACD","#ADD8E6","#F08080","#E0FFFF","#FAFAD2","#D3D3D3","#90EE90","#FFB6C1","#FFA07A",
					"#20B2AA","#87CEFA","#778899","#0C4DE","#FFFFE0","#00FF00","#32CD32","#FAF0E6","#FF00FF","#800000","#66CDAA",
					"#0000CD","#BA55D3","#9370D8","#3CB371","#7B68EE","#00FA9A","#48D1CC","#C71585","#191970","#F5FFFA","#FFE4E1",
					"#FFE4B5","#FFDEAD","#000080","#FDF5E6","#808000","#6B8E23","#FFA500","#FF4500","#DA70D6","#EEE8AA","#98FB98",
					"#AFEEEE","#D87093","#FFEFD5","#FFDAB9","#CD853F","#FFC0CB","#DDA0DD","#B0E0E6","#800080","#FF0000","#BC8F8F",
					"#4169E1","#8B4513","#FA8072",
				],

				//把字符串以“空格”分割成字符串数组
				nameSplit = String(name).split(' '),
				initials, charIndex, colourIndex, canvas, context, dataURI;

			//截取第一个字符，如果值为Flse，则用“？”代替
			if (nameSplit.length == 1) {
				initials = nameSplit[0] ? nameSplit[0].charAt(0):'?';
			} else {
				initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
			}
			//字母转大写
			initials = initials.toUpperCase();

			if (w.devicePixelRatio) {
				size = (size * w.devicePixelRatio);
			}
				
			//绘制图像
			charIndex     = (initials == '?' ? 72 : initials.charCodeAt(0)) - 64;
			colourIndex   = charIndex % 20;
			canvas        = d.createElement('canvas');
			canvas.width  = size;
			canvas.height = size;
			context       = canvas.getContext("2d");
			
			//设置颜色及字体等
			context.fillStyle = color ? color : colours[colourIndex - 1];
			context.fillRect (0, 0, canvas.width, canvas.height);
			context.font = Math.round(canvas.width/2)+"px Arial";//文本字体
			context.textAlign = "center";//文本居中
			context.fillStyle = "#FFF";//文本字体颜色
			context.fillText(initials, size / 2, size / 1.5);

			dataURI = canvas.toDataURL();
			canvas  = null;

			return dataURI;
		}

		Dkpic.transform = function() {

			Array.prototype.forEach.call(d.querySelectorAll('img[Dkpic]'), function(img, name, color) {
				name = img.getAttribute('Dkpic');
				color = img.getAttribute('color');
				img.src = Dkpic(name, img.getAttribute('width'), color);
				img.removeAttribute('Dkpic');
				img.setAttribute('alt', name);
			});
		};

		// 支持AMD
		if (typeof define === 'function' && define.amd) {			
			define(function () { return Dkpic; });		
		// 支持CommonJS和Node.js模块
		} else if (typeof exports !== 'undefined') {
			
			// 支持Node.js特定的`module.exports`（这里可以是一个函数）
			if (typeof module != 'undefined' && module.exports) {
				exports = module.exports = Dkpic;
			}

			// 始终支持CommonJS模块1.1.1规范（“exports”不能作为函数）
			exports.Dkpic = Dkpic;
		} else {			
			window.Dkpic = Dkpic;
			d.addEventListener('DOMContentLoaded', function(event) {
				Dkpic.transform();
			});
		}
	})(window, document);
	