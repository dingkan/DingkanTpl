# Dkpic
Dkpic（顶看图片），采用JS获取文本首字符，随机采用颜色为背景生成图片，生成的图片可用于网站人物头像、LOGO等。

## 快速上手

获得 Dkpic 后，将其完整地部署到你的项目目录（或静态资源服务器），你只需要引入下述两个文件：

```
./Dkpic/js/jquery-3.4.1.min.js //提示：如果你的项目中已有jquery，则不用再次重复引入jquery
./Dkpic/js/Dkpic.js
```
这是一个基本的入门页面：
```html
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<title>Dkpic - 顶看</title>

	<style type="text/css">
		body {font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;}
		/* 图形像素 */
		.Dkpic{width:180px; height:180px;}
		/* 圆形头像 */
		.round {border-radius:50%;}
	</style>
</head>
<body>
<h1>Dkpic</h1>
<h3><strong>实例:</strong></h3>

<div>
	<img class="Dkpic" Dkpic="顶">
	<img class="Dkpic" Dkpic="看">
</div>

<div>
	<img class="Dkpic round" Dkpic="欢迎">
	<img class="Dkpic round" Dkpic="迎接">
</div>

<!-- 引入Dkpic -->
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/Dkpic.js"></script>
</body>
</html>
```
<img src="doc/Dkpic.jpg"/>